FROM openjdk:8-jdk-alpine
EXPOSE 8081
ARG JAR_FILE=target/demo-0.0.1.jar
ADD ${JAR_FILE} demo-cicd.jar
ENTRYPOINT ["java","-jar","/demo-cicd.jar"]
